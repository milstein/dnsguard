package org.sdnhub.dnsguard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.dougharris.dns.RFC1035;

import org.opendaylight.controller.hosttracker.IfHostListener;
import org.opendaylight.controller.hosttracker.hostAware.HostNodeConnector;
import org.opendaylight.controller.sal.action.Action;
import org.opendaylight.controller.sal.action.Controller;
import org.opendaylight.controller.sal.core.Node;
import org.opendaylight.controller.sal.core.NodeConnector;
import org.opendaylight.controller.sal.core.Property;
import org.opendaylight.controller.sal.core.UpdateType;
import org.opendaylight.controller.sal.flowprogrammer.Flow;
import org.opendaylight.controller.sal.flowprogrammer.IFlowProgrammerService;
import org.opendaylight.controller.sal.inventory.IListenInventoryUpdates;
import org.opendaylight.controller.sal.match.Match;
import org.opendaylight.controller.sal.match.MatchField;
import org.opendaylight.controller.sal.match.MatchType;
import org.opendaylight.controller.sal.packet.Ethernet;
import org.opendaylight.controller.sal.packet.IDataPacketService;
import org.opendaylight.controller.sal.packet.IListenDataPacket;
import org.opendaylight.controller.sal.packet.IPv4;
import org.opendaylight.controller.sal.packet.Packet;
import org.opendaylight.controller.sal.packet.PacketResult;
import org.opendaylight.controller.sal.packet.RawPacket;
import org.opendaylight.controller.sal.packet.UDP;
import org.opendaylight.controller.sal.utils.EtherTypes;
import org.opendaylight.controller.sal.utils.NetUtils;
import org.opendaylight.controller.sal.utils.Status;
import org.opendaylight.controller.switchmanager.ISwitchManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DnsGuard implements IListenDataPacket, IListenInventoryUpdates, IfHostListener, IDnsGuard{
	
	private Persistence database;
	static final String dbserver = "192.168.87.137";
	static final String dbname = "dnsspy";
	static final String user = "root";
	static final String passwd = "password";
	static final int dbport = 3306;
	
	
	protected static final Logger log = LoggerFactory
			.getLogger(DnsGuard.class);
	
	private IDataPacketService dataPacketService = null;
	
	void setDataPacketService(IDataPacketService s) {
		this.dataPacketService = s;
	}

	void unsetDataPacketService(IDataPacketService s) {
		if (this.dataPacketService == s) {
			this.dataPacketService = null;
		}
	}
	
	private ISwitchManager swManager = null;
	
	void setSwitchManager(ISwitchManager s) {
		log.info("set ISwitchManager");
		this.swManager = s;
	}

	void unsetSwitchManager(ISwitchManager s) {
		if (this.swManager == s) {
			this.swManager = null;
		}
	}
	
	private IFlowProgrammerService programmer = null;
	
    public void setFlowProgrammerService(IFlowProgrammerService s)
    {
        this.programmer = s;
    }

    public void unsetFlowProgrammerService(IFlowProgrammerService s) {
        if (this.programmer == s) {
            this.programmer = null;
        }
    }
	
	
	@Override
	public PacketResult receiveDataPacket(RawPacket inPkt) {
		// TODO Auto-generated method stub
		
		//log.info("packet arrived");
		
		Packet etherpack = this.dataPacketService.decodeDataPacket(inPkt);
		
		if (etherpack instanceof Ethernet) {
			
			Packet ippack = etherpack.getPayload();
			
			if (ippack instanceof IPv4) {
				 
				Packet udppackt = ippack.getPayload();
	
				if (udppackt instanceof UDP) {
					
					//  & 0xFFFF
					short sport = ((UDP)udppackt).getSourcePort();
					short dport = ((UDP)udppackt).getDestinationPort();
					
					if(sport == 53 || dport == 53){
						
						int sip = ((IPv4) ippack).getSourceAddress();
						int dip = ((IPv4) ippack).getDestinationAddress();
						 
						byte[] dnspacket = ((UDP)udppackt).getRawPayload();
						
						if(dnspacket != null ){
							 
							RFC1035 dns_msg = new RFC1035(dnspacket);
							
							try{
								dns_msg.parse();
								
								if (dns_msg.qr == false) {
									
									log.info("DNS REPLY len: {}  from: {} to: {} dport: {}", dnspacket.length, NetUtils.getInetAddress(sip).toString(), NetUtils.getInetAddress(dip).toString(), dport);
									
									DnsReply reply = new DnsReply(dns_msg);
									 
									if (dns_msg.getCountAnswer() > 0) {
										reply.setPacketHeaders(NetUtils.getInetAddress(sip).toString().replace("/",""), NetUtils.getInetAddress(dip).toString().replace("/",""), sport, dport);
										database.save(reply);
									}
								}

							}catch(Exception ex){
								
								log.error(ex.getLocalizedMessage());
							} 
						}
					}
				}
			}
		}

		return PacketResult.KEEP_PROCESSING;
	}

	void init() {
		// TODO: add rule to forward dns to the controller
		
		// connect to db
		database = new Persistence(dbserver, dbport, dbname, user, passwd);
		
		boolean dbconx = database.Connect();
		
		log.info("Database connected: {} ", dbconx);
		
	}

	@Override
	public String echo(String in) {
		return in + " from class";
	}

	@Override
	public List<String> lazyresolv(String appIp) {
		
		//select distinct request  FROM dnsspy.bulkreply where data = '61.211.161.1'
		if (database.Connect()){
			return database.lazyresolv(appIp);
		}
		
		return new ArrayList<String>();
	}
	
	@Override
	public List<String> appsbyip(String sourceIp) {
		
		//select distinct request  FROM dnsspy.bulkreply where data = '61.211.161.1'
		if (database.Connect()){
			return database.appsbyip(sourceIp);
		}
		
		return new ArrayList<String>();
	}

	@Override
	public void updateNode(Node node, UpdateType type, Set<Property> props) {
		// TODO Auto-generated method stub
		// log.info("updateNode, NodeType {} Update {} Prop {}", node.getID().toString(), type.toString(), props.toString());
		
		if( type == UpdateType.ADDED){
			 
			 for (Iterator<Property> it = props.iterator(); it.hasNext(); ) {
				  
				 Property pval = it.next();
				 
				 //log.info("updateNode {} - {}", pval.getName().toString(), pval.getStringValue());
				 
				 if(pval.getName() == "macAddress"){
					 
					 log.info("sw is up");
					 
					 // forward dns traffic to controller: TP_DST, (short)53
					 Match match = new Match();
					 match.setField( new MatchField(MatchType.DL_TYPE, EtherTypes.IPv4.shortValue()));
					 match.setField( new MatchField(MatchType.NW_PROTO, (byte)17) );
					 match.setField( new MatchField(MatchType.TP_DST, (short)53) ); 
					
					 List<Action> actions = new ArrayList<Action>();
					 actions.add(new Controller());
					 
			         Flow flow = new Flow(match, actions);
			         flow.setIdleTimeout((short) 0);
			         flow.setHardTimeout((short) 0);
			         flow.setPriority((short) 32769);

			         // Modify the flow on the network node
			         Status status = programmer.addFlow(node, flow);

			         if (!status.isSuccess()) {
			            log.warn("SDN Plugin failed to program the flow: {}. The failure is: {}",
			                    flow, status.getDescription());
			         }
			         
					 
					 // forward dns traffic to controller: TP_SRC, (short)53
//					 match = new Match();
//					 match.setField( new MatchField(MatchType.DL_TYPE, EtherTypes.IPv4.shortValue()));
//					 match.setField( new MatchField(MatchType.NW_PROTO, (byte)17) );
//					 match.setField( new MatchField(MatchType.TP_SRC, (short)53) ); 
//					
//					 actions = new ArrayList<Action>();
//					 actions.add(new Controller());
//					 
//			         flow = new Flow(match, actions);
//			         flow.setIdleTimeout((short) 0);
//			         flow.setHardTimeout((short) 0);
//			         flow.setPriority( (short) 32769);
//
//			         // Modify the flow on the network node
//			         status = programmer.addFlow(node, flow);
//
//			         if (!status.isSuccess()) {
//			            log.warn("SDN Plugin failed to program the flow: {}. The failure is: {}",
//			                    flow, status.getDescription());
//			         }
			          
//					 if(this.swManager != null){
//						 Set<NodeConnector> nconx = this.swManager.getNodeConnectors(node);
//						 log.info("got nconx");
//					 }
				 }
				  
			 }
			 
		}
		
	}

	@Override
	public void updateNodeConnector(NodeConnector nodeConnector,
			UpdateType type, Set<Property> props) {
		// TODO Auto-generated method stub
	}

	@Override
	public void hostListener(HostNodeConnector host) {
		// TODO Auto-generated method stub
		
		log.info("new host {}", host.getNetworkAddressAsString());
	
	}

	@Override
	public Map<String, String> getViolators() {
		// TODO Auto-generated method stub
		
		//select distinct request  FROM dnsspy.bulkreply where data = '61.211.161.1'
		if (database.Connect()){
			return database.getViolators();
		}
		
		return new HashMap<String,String>(); 
	}

	@Override
	public String setLocalDnsServer(String local_dns) {
		// TODO Auto-generated method stub
		
		//select distinct request  FROM dnsspy.bulkreply where data = '61.211.161.1'
		if (database.Connect()){
			return database.setLocalDnsServer(local_dns);
		}
		
		return new String("Error updating data");
	}

	@Override
	public String getLocalDnsServer() {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		
		//select distinct request  FROM dnsspy.bulkreply where data = '61.211.161.1'
		if (database.Connect()){
			return database.getLocalDnsServer();
		}
		
		return new String("Error getting data");
	}
 
 
}
