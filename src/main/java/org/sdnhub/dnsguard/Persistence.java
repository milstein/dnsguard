package org.sdnhub.dnsguard;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.dougharris.dns.ResourceRecord;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Luis Chiang
 * This class is in charge to create a connection to the database and keep a 
 * memory buffer to store the dns responses. 
 * 
 */
public class Persistence implements IDnsGuard {
	protected static final Logger log = LoggerFactory
			.getLogger(Persistence.class);
	
	private String dbdriver = "com.mysql.jdbc.Driver";
	private String dbserver,dbname,user,passwd;
	private int dbport;
	private Connection connection;
	
	private static final int INTERNAL_BUFFER = 100;
	private static final int INTERNAL_BUFFER_MAX = 200;
	
	private List<ResourceRecord> internal_buffer_rr = new ArrayList<ResourceRecord>(INTERNAL_BUFFER_MAX);
	private List<DnsReply> internal_buffer_dnsreply = new ArrayList<DnsReply>(INTERNAL_BUFFER_MAX);
	
	public Persistence(String dbserver, int dbport, String dbname, String user, String passwd){
		
		this.dbserver = dbserver;
		this.dbport = dbport;
		this.dbname = dbname;
		this.user = user;
		this.passwd = passwd;
		
	}
	
	public Boolean Connect(){
		
		try {
			
			// fix in case of class not found, edit file: \main\src\assemble\bin.xml
			// and add the line: <include>mysql:mysql-connector-java</include>
			
	          Class.forName(dbdriver).newInstance();
	          String url = "jdbc:mysql://" + dbserver +":" + String.valueOf(dbport) +"/" + dbname;
	          
	          this.connection = DriverManager.getConnection(url,user,passwd);
	          
	          return true;
	           
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	public void save(List<ResourceRecord> reply_answers){

		// List<ResourceRecord> reply_answers = dnsreply.getAllAnswers();
		
		this.internal_buffer_rr.addAll(reply_answers);
		
		log.info("rr internal buffer size {}", this.internal_buffer_rr.size());
		
		if (this.internal_buffer_rr.size() >= INTERNAL_BUFFER){
			// flush local cache
			
			try {
				
				Statement statement = connection.createStatement();
			
			for (ResourceRecord rr: internal_buffer_rr) {
				
				// TODO: improve security
				try{
					
				    String sql = "INSERT INTO bulkreply (tstamp, request, type, ttl, len, data) values (NOW(), '" + rr.getName() + "', " + rr.getType() + ", " + rr.getTTL() + ", " + rr.getLength() + ", '" + rr.dataToString() +"');";			    
				    statement.addBatch(sql);
			    
				}catch(Exception ex){
					ex.printStackTrace();
				}
				
			}
			
			statement.executeBatch();
			statement.close();
			
			this.internal_buffer_rr.clear();
			
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	
	public void save(DnsReply dnsreply){
		// TODO: this functions is not working propertly, should iterate using
		// internal_buffer_dnsreply and save the ip.src ip.dst udp.src udp.dst

		this.internal_buffer_dnsreply.add(dnsreply);

		log.info("dns internal buffer size {}",
				this.internal_buffer_dnsreply.size());

		if (this.internal_buffer_dnsreply.size() >= INTERNAL_BUFFER) {
			// flush local cache
			   flushLocalCache();
		}

	}
	
	private void flushLocalCache(){
		
		if(this.internal_buffer_dnsreply.size() > 0){
			
			try {
	
				Statement statement = connection.createStatement();
	
				for (DnsReply tmpReply : this.internal_buffer_dnsreply) {
	
					List<ResourceRecord> tmp_rr = tmpReply.getAllAnswers();
	
					for (ResourceRecord rr : tmp_rr) {
	
						// TODO: improve security
						try {
	
							String sql = "INSERT INTO bulkreply (tstamp, request, type, ttl, len, data, ip_src, ip_dst, udp_src, udp_dst) values ( NOW(), '"
									+ rr.getName()
									+ "', "
									+ rr.getType()
									+ ", "
									+ rr.getTTL()
									+ ", "
									+ rr.getLength()
									+ ", '"
									+ rr.dataToString() + "','" + tmpReply.getSrc_ip() +  "','" + tmpReply.getDst_ip() + "'," + tmpReply.getUdp_src() + "," + tmpReply.getUdp_dst() + ");";
							statement.addBatch(sql);
	
						} catch (Exception ex) {
							ex.printStackTrace();
						}
	
					}
				}
	
				statement.executeBatch();
				statement.close();
	
				this.internal_buffer_rr.clear();
	
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public List<String> lazyresolv(String appIp) {
	
		List<String> results = new ArrayList<String>();
		
		try {

			Statement statement = connection.createStatement();

			String sql = "select distinct request  FROM dnsspy.bulkreply where data = '" + appIp + "'";
			
			ResultSet rs = statement.executeQuery(sql);
			
			while (rs.next()){
				
				results.add( rs.getString(1) );
			}
			
			rs.close();
			statement.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}

	public List<String> appsbyip(String sourceIp) {
		
		List<String> results = new ArrayList<String>();
		
		try {

			Statement statement = connection.createStatement();

			String sql = "select distinct request FROM dnsspy.bulkreply where ip_dst = '" + sourceIp + "'";
			
			ResultSet rs = statement.executeQuery(sql);
			
			while (rs.next()){
				
				results.add( rs.getString(1) );
			}
			
			rs.close();
			statement.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}
	
	public Map<String, String> getViolators() {
		
		Map<String, String> results = new HashMap<String, String>();
		 
		try {

			Statement statement = connection.createStatement();
			
			String sql = "select ip_dst, tstamp FROM dnsspy.bulkreply where ip_src not in (select local_dns_ip from dnsspy.config) group by ip_dst";
			
			ResultSet rs = statement.executeQuery(sql);
			
			while (rs.next()){
				
				results.put( rs.getString(1),  rs.getString(2) );
			}
			
			rs.close();
			statement.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}

	public void save(DnsRequest dnsrequest){
		// TODO:
		
	}
	
	public DnsReply resolve(DnsRequest dnsrequest){
		// TODO: 
		DnsReply dnsreply = null;
		
		
		return dnsreply;
		
	}

	@Override
	public String echo(String in) {
		// TODO Auto-generated method stub
		return in + "not hit the db";
	}

	@Override
	public String setLocalDnsServer(String local_dns) {
		// TODO Auto-generated method stub
		 
		Statement statement;
		
		try {
			statement = connection.createStatement();
			
			String sql = "update config set local_dns_ip = '" + local_dns + "' where idconfig = 1";
			
			// false if is an update
			if( statement.execute(sql) == false){
				
				return "Updated Ok";
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 
		return "Failed to Update";
	}

	@Override
	public String getLocalDnsServer() {
		// TODO Auto-generated method stub
		
		String results = new String();
		try {

			Statement statement = connection.createStatement();
			
			String sql = "select local_dns_ip from config where idconfig = 1";
			
			ResultSet rs = statement.executeQuery(sql);
			
			rs.next();
			results = rs.getString(1); 
			
			rs.close();
			statement.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	} 
}
