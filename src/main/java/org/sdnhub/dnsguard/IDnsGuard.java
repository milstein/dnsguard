package org.sdnhub.dnsguard;

import java.util.List;
import java.util.Map;

public interface IDnsGuard {

	public String echo (String in);
	
	public List<String> lazyresolv (String appIp);
	
	public List<String> appsbyip (String sourceIp);
	
	public Map<String, String> getViolators ();
	
	public String setLocalDnsServer (String local_dns);
	
	public String getLocalDnsServer ();
	
}
