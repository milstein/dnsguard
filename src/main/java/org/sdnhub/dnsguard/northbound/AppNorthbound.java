
package org.sdnhub.dnsguard.northbound;

import org.codehaus.enunciate.jaxrs.StatusCodes;
import org.codehaus.enunciate.jaxrs.TypeHint;
import org.opendaylight.controller.sal.utils.ServiceHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import org.codehaus.enunciate.jaxrs.ResponseCode;
import org.opendaylight.controller.northbound.commons.RestMessages;
import org.opendaylight.controller.northbound.commons.exception.ServiceUnavailableException;
import org.opendaylight.controller.northbound.commons.exception.UnauthorizedException;
import org.opendaylight.controller.northbound.commons.utils.NorthboundUtils;
import org.opendaylight.controller.sal.authorization.Privilege;
import org.opendaylight.controller.sal.utils.Status;
import org.sdnhub.dnsguard.DnsGuard;
import org.sdnhub.dnsguard.IDnsGuard;

import com.google.gson.Gson;

/**
 * Northbound REST API
 *
 * This entire web class can be accessed via /northbound prefix as specified in
 * web.xml
 *
 * <br>
 * <br>
 * Authentication scheme : <b>HTTP Basic</b><br>
 * Authentication realm : <b>opendaylight</b><br>
 * Transport : <b>HTTP and HTTPS</b><br>
 * <br>
 * HTTPS Authentication is disabled by default.
 */
@Path("/")
public class AppNorthbound {
    @Context
    private UriInfo _uriInfo;
    private String username;

    @Context
    public void setSecurityContext(SecurityContext context) {
        if (context != null && context.getUserPrincipal() != null) {
            username = context.getUserPrincipal().getName();
        }
    }

    protected String getUserName() {
        return username;
    }
    
    /**
     * 
     * Sample REST API call
     * 
     * @return A response string
     * 
     *         <pre>
     * Example:
     * 
     * Request URL:
     * http://127.0.0.1:8080/dnsguard/northbound/test
     * 
     * Response body in XML:
     * &lt;?xml version="1.0" encoding="UTF-8" standalone="yes"?&gt;
     * Sample Northbound API
     * 
     * Response body in JSON:
     * Sample Northbound API
     * </pre>
     */
    @Path("/test")
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @TypeHint(String.class)
    @StatusCodes()
    public String getTest() {
        String result = "<xml><output>Sample Northbound API from module ATF</output></xml>";
        return result;
    }
    
    /**
     * 
     * Sample REST API call
     * 
     * @return A response string
     * 
     *         <pre>
     * Example:
     * 
     * Request URL:
     * http://127.0.0.1:8080/dnsguard/northbound/echo/{echo}
     * 
     * Response body in XML:
     * &lt;?xml version="1.0" encoding="UTF-8" standalone="yes"?&gt;
     * Sample Northbound API
     * 
     * Response body in JSON:
     * Sample Northbound API
     * </pre>
     */
    @Path("/echo/{echo}")
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @TypeHint(String.class)
    @StatusCodes()
    public String getEcho(@PathParam("echo") String echo) {
    	
    	if (!NorthboundUtils.isAuthorized(getUserName(), "default", Privilege.READ, this)) {
            throw new UnauthorizedException("User is not authorized to perform this operation");
        }
    	
    	IDnsGuard dnshandler = (IDnsGuard) ServiceHelper.getInstance(IDnsGuard.class, "default", this);
    	
        if (dnshandler == null) {
            throw new ServiceUnavailableException("DnsHandler Service " + RestMessages.SERVICEUNAVAILABLE.toString());
        }

        String result = "<xml><output>Query from module ATF " + dnshandler.echo(echo)  + " </output></xml>";
        
        return result;

   }

    /**
     * 
     * Sample REST API call
     * 
     * @return A response string
     * 
     *         <pre>
     * Example:
     * 
     * Request URL:
     * http://127.0.0.1:8080/dnsguard/northbound/resolv
     * 
     * Response body in XML:
     * &lt;?xml version="1.0" encoding="UTF-8" standalone="yes"?&gt;
     * Sample Northbound API
     * 
     * Response body in JSON:
     * Sample Northbound API
     * </pre>
     */
    @Path("/resolv/{IpFrom}/{AppIp}")
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON })
    @TypeHint(String.class)
    @StatusCodes()
    public String getQuery(@PathParam("IpFrom") String ipFrom, @PathParam("AppIp") String appIP) {
    	
    	if (!NorthboundUtils.isAuthorized(getUserName(), "default", Privilege.READ, this)) {
            throw new UnauthorizedException("User is not authorized to perform this operation");
        }
    	
    	IDnsGuard dnshandler = (IDnsGuard) ServiceHelper.getInstance(IDnsGuard.class, "default", this);
    	
        if (dnshandler == null) {
            throw new ServiceUnavailableException("DnsHandler Service " + RestMessages.SERVICEUNAVAILABLE.toString());
        }

        String result = "<xml><output>ipFrom: " + dnshandler.echo(ipFrom)  + " appIP: " + dnshandler.echo(appIP)  + " </output></xml>";
        
        return result;
    }
    
    /**
     * 
     * Sample REST API call
     * 
     * @return A response string
     * 
     *         <pre>
     * Example:
     * 
     * Request URL:
     * http://127.0.0.1:8080/dnsguard/northbound/lazyresolv/appIp
     * 
     * Response body in XML:
     * &lt;?xml version="1.0" encoding="UTF-8" standalone="yes"?&gt;
     * Sample Northbound API
     * 
     * Response body in JSON:
     * Sample Northbound API
     * </pre>
     */
    @Path("/lazyresolv/{appIp}")
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON })
    @TypeHint(String.class)
    @StatusCodes()
    public String lazyresolv(@PathParam("appIp") String appIp) {
    	
    	if (!NorthboundUtils.isAuthorized(getUserName(), "default", Privilege.READ, this)) {
            throw new UnauthorizedException("User is not authorized to perform this operation");
        }
    	
    	IDnsGuard dnshandler = (IDnsGuard) ServiceHelper.getInstance(IDnsGuard.class, "default", this);
    	
        if (dnshandler == null) {
            throw new ServiceUnavailableException("DnsHandler Service " + RestMessages.SERVICEUNAVAILABLE.toString());
        }


        String json = new Gson().toJson( dnshandler.lazyresolv(appIp) );
        
        return json;
    }
    
    /***
     * 
     * @param http://127.0.0.1:8080/dnsguard/northbound/appsbyip/{sourceIp}
     * @return The domains visited by an internal IP
     */
    @Path("/appsbyip/{sourceIp}")
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON })
    @TypeHint(String.class)
    @StatusCodes()
    public String appsbyip(@PathParam("sourceIp") String sourceIp) {
    	
    	if (!NorthboundUtils.isAuthorized(getUserName(), "default", Privilege.READ, this)) {
            throw new UnauthorizedException("User is not authorized to perform this operation");
        }
    	
    	IDnsGuard dnshandler = (IDnsGuard) ServiceHelper.getInstance(IDnsGuard.class, "default", this);
    	
        if (dnshandler == null) {
            throw new ServiceUnavailableException("DnsHandler Service " + RestMessages.SERVICEUNAVAILABLE.toString());
        }

        String json = new Gson().toJson( dnshandler.appsbyip(sourceIp) );
        
        return json;
    }

    /**
     * 
     * Returns the violators of the local dns server
     * 
     * @return A response string
     * 
     *         <pre>
     * Example:
     * 
     * Request URL:
     * http://127.0.0.1:8080/dnsguard/northbound/getviolators
     * 
     * Response body in XML:
     * &lt;?xml version="1.0" encoding="UTF-8" standalone="yes"?&gt;
     * Sample Northbound API
     * 
     * Response body in JSON:
     * Sample Northbound API
     * </pre>
     */
    @Path("/getviolators")
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON })
    @TypeHint(String.class)
    @StatusCodes()
    public String getViolators() {
    	
    	if (!NorthboundUtils.isAuthorized(getUserName(), "default", Privilege.READ, this)) {
            throw new UnauthorizedException("User is not authorized to perform this operation");
        }
    	
    	IDnsGuard dnshandler = (IDnsGuard) ServiceHelper.getInstance(IDnsGuard.class, "default", this);
    	
        if (dnshandler == null) {
            throw new ServiceUnavailableException("DnsHandler Service " + RestMessages.SERVICEUNAVAILABLE.toString());
        }


        String json = new Gson().toJson( dnshandler.getViolators() );
        
        return json;
    }
 
    
    /***
     * 
     * @param http://127.0.0.1:8080/dnsguard/northbound/setlocaldns/{localdns}
     * @return The domains visited by an internal IP
     */
    @Path("/setlocaldns/{localdns}")
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON })
    @TypeHint(String.class)
    @StatusCodes()
	public String setLocalDnsServer(@PathParam("localdns") String local_dns) {
		
		// TODO Auto-generated method stub
    	if (!NorthboundUtils.isAuthorized(getUserName(), "default", Privilege.READ, this)) {
            throw new UnauthorizedException("User is not authorized to perform this operation");
        }
    	
    	IDnsGuard dnshandler = (IDnsGuard) ServiceHelper.getInstance(IDnsGuard.class, "default", this);
    	
        if (dnshandler == null) {
            throw new ServiceUnavailableException("DnsHandler Service " + RestMessages.SERVICEUNAVAILABLE.toString());
        }


        String json = new Gson().toJson( dnshandler.setLocalDnsServer(local_dns) );
        
        return json;
	}
    
    /***
     * 
     * @param http://127.0.0.1:8080/dnsguard/northbound/getlocaldns
     * @return The domains visited by an internal IP
     */
    @Path("/getlocaldns")
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON })
    @TypeHint(String.class)
    @StatusCodes()
	public String getLocalDnsServer() {
		
		// TODO Auto-generated method stub
    	if (!NorthboundUtils.isAuthorized(getUserName(), "default", Privilege.READ, this)) {
            throw new UnauthorizedException("User is not authorized to perform this operation");
        }
    	
    	IDnsGuard dnshandler = (IDnsGuard) ServiceHelper.getInstance(IDnsGuard.class, "default", this);
    	
        if (dnshandler == null) {
            throw new ServiceUnavailableException("DnsHandler Service " + RestMessages.SERVICEUNAVAILABLE.toString());
        }


        String json = new Gson().toJson( dnshandler.getLocalDnsServer() );
        
        return json;
	}
    
    
}
